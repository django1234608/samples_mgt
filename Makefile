help: ## display this help message
	@echo "Please use \`make <target>' where <target> is one of"
	@grep '^[a-zA-Z]' $(MAKEFILE_LIST) | sort | awk -F ':.*?## ' 'NF==2 {printf "\033[36m %-25s\033[0m %s\n", $$1, $$2}'


compose:
	podman-compose down
	podman stop -a && podman rm -a -f && podman rmi -a
	podman volume rm --all
	podman-compose up --build --force-recreate --abort-on-container-exit

clean:
	DJANGO_SETTINGS_MODULE=project.settings.test \
	python manage.py reset_db
	rm -rf .venv
	find -iname "*.pyc" -delete

reset_db:
	DJANGO_SETTINGS_MODULE=project.settings.test \
	python manage.py reset_db

virtualenv:
	python3 -m venv .venv --upgrade-deps
	.venv/bin/python -m pip install -r requirements/test.in

migrate:
	DJANGO_SETTINGS_MODULE=project.settings.test \
	python manage.py makemigrations && \
	DJANGO_SETTINGS_MODULE=project.settings.test \
	python manage.py migrate

run:
	DJANGO_SETTINGS_MODULE=project.settings.test \
	python manage.py runserver

test:
	DJANGO_SETTINGS_MODULE=project.settings.test \
	pytest -s

makemessages:
	DJANGO_SETTINGS_MODULE=project.settings.test \
	django-admin makemessages --all --ignore=env

compilemessages:
	DJANGO_SETTINGS_MODULE=project.settings.test \
	django-admin compilemessages --ignore=env

delmigrations:
	rm applications/patients/migrations/00*
	rm applications/places/migrations/00*
	rm applications/samples/migrations/00*
	rm applications/core/migrations/00*
	git rm applications/patients/migrations/00*
	git rm applications/places/migrations/00*
	git rm applications/samples/migrations/00*
	git rm applications/core/migrations/00*

ruff:
	ruff format
