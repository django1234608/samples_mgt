import datetime
import io

import polars as pl
from django.apps import apps
from django.db import connection
from django.http import FileResponse
from drf_spectacular.utils import extend_schema
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from rest_framework.response import Response
from rest_framework.views import APIView

# Create your views here.


class ExampleView(APIView):
    def get(self, request, format=None):
        content = {
            "user": str(request.user),  # `django.contrib.auth.User` instance.
            "auth": str(request.auth),  # None
        }
        return Response(content)


# All my applications should be able to return a cvs file representing the tables
def select_data_dict():
    # all Django installed apps
    installed_apps = apps.get_models()
    # all apps developped by me
    my_apps = set(
        [
            x.__module__.split(".")[1]
            for x in apps.get_models()
            if x.__module__.startswith("applications")
        ]
    )
    # I need to create a string that will be concatenated to the query string below
    table_names = map(lambda x: "t.table_name like '" + x + "_%'", my_apps)
    or_query_tables = " or ".join(table_names)
    # query string
    query = (
        """
            WITH tables AS (
                SELECT c.oid,
                       ns.nspname as schema_name,
                       c.relname as table_name,
                       d.description as table_description,
                       pg_get_userbyid(c.relowner) AS table_owner
                FROM pg_catalog.pg_class AS c
                JOIN pg_catalog.pg_namespace AS ns
                  ON c.relnamespace = ns.oid
                LEFT JOIN pg_catalog.pg_description d
                  on c.oid = d.objoid
                 and d.objsubid = 0
                 WHERE ns.nspname not in ('pg_catalog')
            )
            SELECT split_part(c.table_name, '_', 2) AS "table name",
                   c.column_name AS "column name",
                   c.data_type as "column type",
                   c.character_maximum_length as "column max length",
                   c.column_default as "default value",
                   c.is_nullable as "accept null value",
                   d.description as "column description"
            from tables t
            join information_schema.columns c
                on c.table_schema = t.schema_name
               and c.table_name = t.table_name
            left join pg_catalog.pg_description d
               ON d.objoid = t.oid
              AND d.objsubid = c.ordinal_position
              AND d.objsubid > 0
            where 1=1
            and
            t.table_name not like '%historical%'
            and (
         """
        + or_query_tables
        + """
            )
            order by
                t.table_name
        """
    )

    with connection.cursor() as cursor:
        cursor.execute(query)
        rows = cursor.fetchall()
        columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in rows]


@extend_schema()
@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def data_dictionary(request, *args, **kwargs):
    """
    A view that returns a list of all the tables inside the database
    """
    df = pl.DataFrame(select_data_dict())
    csv_buffer = io.BytesIO()
    df.write_csv(csv_buffer)
    csv_buffer.seek(0)
    return FileResponse(
        csv_buffer,
        filename=f"data-dictionary-{datetime.datetime.now().strftime('%Y-%m-%d')}.csv",
        as_attachment=True,
        content_type="text/csv",
        status=200,
    )
