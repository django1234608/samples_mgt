"""
URL configuration for core Rest API v1
"""

from django.urls import path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from applications.core.rest_api.v1 import views

app_name = "applications.core"

urlpatterns = [
    # SCHEMA
    path("schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="applications.core:v1:schema"),
        name="swagger-ui",
    ),
    # JWT
    path("token/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
]

urlpatterns += [
    # url for the data dictionary,
    path("data/dictionary", views.data_dict, name="database-data-dictionary"),
    # Add your urls here...
]
