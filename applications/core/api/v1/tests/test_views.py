import csv
import io

import pytest
from django.apps import apps
from rest_framework.test import APIRequestFactory

from applications.core.api.v1 import views

core_config = apps.get_app_config("core")


@pytest.mark.django_db
def test_get_data_dict():
    factory = APIRequestFactory()
    view = views.data_dictionary
    request = factory.get("/api/v1/data/dictionary", content_type="application/json")
    response = view(request)
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/csv"
    assert len(list(response.streaming_content)) > 1
