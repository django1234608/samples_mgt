"""
core all Rest API URLs.
"""

from django.urls import include, path

app_name = "applications.core"

urlpatterns = [path("v1/", include("applications.core.api.v1.urls", namespace="v1"))]
