from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from simple_history.models import HistoricalRecords

# Create your models here.


class BaseModel(models.Model):
    created_at = models.DateTimeField(
        db_index=True,
        default=timezone.now,
        db_comment="Date, the entity was created at",
    )
    updated_at = models.DateTimeField(
        auto_now=True, db_comment="Last time, the entity was modified"
    )

    metadata = models.JSONField(
        null=True,
        blank=True,
        db_comment="Any additional metadata that can be attached to the instance",
    )

    # TODO: Make a choice between LogEntry and HistoricalRecords
    # https://django-simple-history.readthedocs.io/en/latest/querying_history.html
    # history = HistoricalRecords(inherit=True)

    class Meta:
        abstract = True


class Profile(BaseModel):
    """
    Used to extend the default User model.
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    date_of_birth = models.DateField()
    bio = models.TextField()
    lab_name = models.CharField(max_length=30)
    phone_number = models.CharField(max_length=20)
