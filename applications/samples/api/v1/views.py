import datetime
import io

from drf_spectacular.utils import extend_schema
from rest_framework import viewsets

from applications.samples.api.v1.pagination import SamplePagination
from applications.samples.api.v1.serializers import (
    ReadRegistrationSerializer,
    ReadSampleSerializer,
    SampleMaterialSerializer,
    WriteRegistrationSerializer,
    WriteSampleSerializer,
)
from applications.samples.models import Registration, Sample, SampleMaterial


class RegistrationViewSet(viewsets.ModelViewSet):
    def get_serializer_class(self):
        if self.request.method in ["GET"]:
            return ReadRegistrationSerializer
        return WriteRegistrationSerializer

    def get_queryset(self):
        # TODO: filter by ?
        return Registration.objects.all()


class SampleMaterialViewSet(viewsets.ModelViewSet):
    queryset = SampleMaterial.objects.all()
    serializer_class = SampleMaterialSerializer


class SampleViewSet(viewsets.ModelViewSet):
    pagination_class = SamplePagination

    def get_serializer_class(self):
        if self.request.method in ["GET"]:
            return ReadSampleSerializer
        return WriteSampleSerializer

    def get_queryset(self):
        # TODO: filter by ?
        return Sample.objects.all()
