from rest_framework import serializers

from applications.samples.models import Registration, Sample, SampleMaterial


class ReadRegistrationSerializer(serializers.ModelSerializer):
    # we should return the Registration + Sample + SampleCriterias
    class Meta:
        model = Registration
        fields = "__all__"


class WriteRegistrationSerializer(serializers.ModelSerializer):
    # manage fields for Registration
    # manage fields for SampleCriteriaMapping
    # To create a Registration, we need:
    # a Sample, a list of Criterias
    #
    class Meta:
        model = Registration
        fields = "__all__"


class SampleMaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = SampleMaterial
        fields = "__all__"


class ReadSampleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sample
        fields = "__all__"


class WriteSampleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sample
        fields = "__all__"
