import datetime

import pytest
from django.apps import apps
from django.contrib.auth import get_user_model

from applications.samples.models import (
    Analysis,
    Criteria,
    Laboratory,
    LaboratoryUnit,
    Project,
    Registration,
    Sample,
    SampleCriteriaMapping,
    SampleLaboratoryMapping,
    SampleMaterial,
    SampleStatus,
)

samples_config = apps.get_app_config("samples")


@pytest.fixture
def foo():
    pass


# create a user
@pytest.fixture
def one_user():
    # create a scientist, a plate type and a plate
    User = get_user_model()
    user = User.objects.create_user(
        username="Nsukami",
        email="ptrck@nskm.xyz",
        password="pAssw0&d",
    )
    return user


# create 3 Sample materials
@pytest.fixture
def sample_materials():
    materials = [
        ("HUMAN", "BLOOD", "blood sample taken from a human being"),
        ("HUMAN", "SPUTUM", "sputum sample taken from a human being"),
        ("HUMAN", "PLEURAL_FLUID", "pleural fluid taken from a human being"),
        ("OTHER", "OTHER", "other"),
    ]
    for c, s, d in materials:
        SampleMaterial.objects.create(category=c, subcategory=s, description=d)
    return SampleMaterial.objects.all()


# project
@pytest.fixture
def one_project():
    project = Project.objects.create(
        name="First Project",
        description="First Project Description",
        start_date=datetime.datetime.today(),
        end_date=datetime.datetime.today(),
        status="ACTIVE",
    )
    return project


# status
@pytest.fixture
def sample_statuses():
    statuses = [
        "RECEIVED",
        "PENDING",
        "ARCHIVED",
        "TESTED",
        "ACCEPTED",
        "REJECTED",
    ]
    for status in statuses:
        SampleStatus.objects.get_or_create(name=status)
    return SampleStatus.objects.all()


# laboratory
@pytest.fixture
def one_laboratory():
    # create one Laboratory and one Laboratory Unit
    lab1 = Laboratory.objects.create(
        name="First Laboratory",
        lab_id="lab unique identifier",
        status="ACTIVE",
        phone="+221781450048",
        email="ptrck@nskm.xyz",
        address="Dakar Senegal",
    )
    return lab1


@pytest.fixture
def one_lab_unit(one_laboratory):
    lab_unit = LaboratoryUnit.objects.create(
        laboratory=one_laboratory,
        name="Lab Unit 1",
    )
    return lab_unit


# criterias
@pytest.fixture
def sample_criterias(one_laboratory, one_lab_unit):
    criterias = [
        (lab_unit1, "COMPLIANCE", "reason 1", 100),
        (lab_unit1, "ACCEPTABILITY", "reason 2", 200),
        (lab_unit1, "REJECTION", "reason 3", 300),
    ]
    for l, c, r, d in criterias:
        Criteria.objects.create(lab_unit=l, classification=c, reason=r, code=d)
    return Criteria.objects.all()


# create Locations
@pytest.fixture
def one_location():
    location, created = Location.objects.get_or_create(
        continent="Africa",
        country="Senegal",
        latitude=14.7167,
        longitude=17.4677,
        details="A particular detail about this location named Senegal",
    )
    return location


# create Sample
@pytest.fixture
def one_sample(
    one_user, sample_statuses, sample_criterias, one_location, sample_material
):
    sample, created = Sample.objects.get_or_create()
    return sample


# create Users ? Patients ?


# create one Registration
@pytest.fixture
def one_registration(one_user):
    # registration = Registration.objects.create(
    #     user=one_user,
    #     reception_date=datetime.datetime.today()
    # )
    # return registration
    pass
