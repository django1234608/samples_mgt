import json

import pytest
from django.apps import apps
from rest_framework.test import APIRequestFactory, force_authenticate

from applications.samples.api.v1 import views
from applications.samples.api.v1.tests.fixtures import (
    one_lab_unit,
    one_laboratory,
    one_registration,
    one_user,
    sample_criterias,
    sample_materials,
    sample_statuses,
)
from applications.samples.models import Sample

samples_config = apps.get_app_config("samples")


@pytest.mark.django_db
def test_get_data_without_being_authenticated():
    factory = APIRequestFactory()
    view = views.SampleViewSet.as_view({"get": "list"})
    request = factory.get("/api/v1/samples/", content_type="application/json")
    response = view(request).render()
    errmsg = "Authentication credentials were not provided."
    assert json.loads(response.content.decode())["detail"] == errmsg
    assert response.status_code == 401
    assert response.headers["Content-Type"] == "application/json"


@pytest.mark.django_db
def test_get_sample_materials(one_user, sample_materials):
    factory = APIRequestFactory()
    view = views.SampleMaterialViewSet.as_view({"get": "list"})
    request = factory.get("/api/v1/samples/materials/", content_type="application/json")
    force_authenticate(request, one_user)
    response = view(request).render()
    content = json.loads(response.content.decode())
    assert len(content) == 4
    assert (
        len(
            set(content[0].keys()).intersection(
                {
                    "subcategory",
                    "created_at",
                    "metadata",
                    "category",
                    "id",
                    "description",
                    "updated_at",
                }
            )
        )
        == 7
    )
    assert not set(content[0].keys()).isdisjoint(
        {"created_at", "source", "metadata", "updated_at", "label", "id", "description"}
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "application/json"


@pytest.mark.django_db
def test_get_samples(one_user):
    factory = APIRequestFactory()
    view = views.SampleViewSet.as_view({"get": "list"})
    request = factory.get("/api/v1/samples/", content_type="application/json")
    force_authenticate(request, one_user)
    response = view(request).render()
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "application/json"


@pytest.mark.django_db
def test_get_registrations(one_user, one_registration):
    factory = APIRequestFactory()
    # TODO
