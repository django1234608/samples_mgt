from rest_framework import pagination


class SamplePagination(pagination.PageNumberPagination):
    page_size = 20
    page_size_query_param = "size"
    max_page_size = 50
    page_query_param = "p"
