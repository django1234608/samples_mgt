"""
URL configuration for samples Rest API v1
"""

from django.urls import path

from applications.samples.api.v1 import views

app_name = "applications.samples"

sample_material_list = views.SampleMaterialViewSet.as_view(
    {"get": "list", "post": "create"}
)
sample_list = views.SampleViewSet.as_view({"get": "list", "post": "create"})
sample_detail = views.SampleViewSet.as_view(
    {
        "get": "retrieve",
        "patch": "partial_update",
    }
)

registration_list = views.RegistrationViewSet.as_view({"get": "list", "post": "create"})
registration_detail = views.RegistrationViewSet.as_view(
    {
        "get": "retrieve",
        "patch": "partial_update",
    }
)

urlpatterns = [
    path("samples/", sample_list, name="sample-list"),
    path("samples/<int:pk>", sample_detail, name="sample-detail"),
    path("samples/materials/", sample_material_list, name="sample-material-list"),
    path("registrations/", registration_list, name="registration-list"),
    path("registrations/<int:pk>", registration_detail, name="registration-detail"),
]
