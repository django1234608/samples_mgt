"""
samples all Rest API URLs.
"""

from django.urls import include, path

app_name = "applications.samples"

urlpatterns = [path("v1/", include("applications.samples.api.v1.urls", namespace="v1"))]
