# Generated by Django 5.0.6 on 2024-07-01 10:44

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("samples", "0001_initial"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="criteria",
            name="priority",
        ),
        migrations.AlterField(
            model_name="samplestatus",
            name="name",
            field=models.CharField(
                choices=[
                    ("RECEIVED", "Received"),
                    ("PENDING", "Pending"),
                    ("ARCHIVED", "Archived"),
                    ("TESTED", "Tested"),
                    ("ACCEPTED", "Accepted"),
                    ("REJECTED", "Rejected"),
                ],
                db_comment="What is the status of the Sample",
                max_length=30,
            ),
        ),
    ]
