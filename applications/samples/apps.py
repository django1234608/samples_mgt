from pathlib import Path

from django.apps import AppConfig
from django.conf import settings


class SamplesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "applications.samples"
    SAMPLES_BASE_DIR = Path(__file__).resolve().parent
    DEFAULT_DATA_DIR = SAMPLES_BASE_DIR / "data"
    DATA_DIR = getattr(settings, "DATA_DIR", DEFAULT_DATA_DIR)
