from io import StringIO

import pytest
from django.apps import apps
from django.core.management import call_command
from rest_framework.test import APIRequestFactory, force_authenticate

from applications.samples.models import (
    Criteria,
    Project,
    Sample,
    SampleMaterial,
    SampleStatus,
)

samples_config = apps.get_app_config("samples")


@pytest.mark.django_db
def test_insert_samples_materials():
    out = StringIO()
    msg = "Successfully inserted Sample materials inside database"
    assert SampleMaterial.objects.count() == 0
    call_command("insert_samples_materials", stdout=out)
    assert SampleMaterial.objects.count() == 4
    assert msg in out.getvalue()


@pytest.mark.django_db
def test_insert_samples_statuses():
    out = StringIO()
    msg = "Successfully inserted one Project and Sample Statuses inside database"
    assert SampleStatus.objects.count() == 0
    assert Project.objects.count() == 0
    call_command("insert_samples_statuses", stdout=out)
    assert Project.objects.count() == 1
    assert SampleStatus.objects.count() == 6
    assert msg in out.getvalue()


@pytest.mark.django_db
def test_insert_samples_criterias():
    out = StringIO()
    msg = "Success inserting Lab, LabUnit & Criterias inside database"
    assert Sample.objects.count() == 0
    call_command("insert_samples_criterias", stdout=out)
    assert Criteria.objects.count() == 3
    assert msg in out.getvalue()
