from django.contrib.auth.models import User
from django.db import models

from applications.core.models import BaseModel


class SampleMaterial(BaseModel):
    category = models.CharField(
        max_length=100,
        db_comment="Organism where the sample is coming from: Human? Animal? Environment?",
    )
    subcategory = models.CharField(
        max_length=100,
        db_comment="Organism where the sample is coming from: Human? Animal? Environment?",
    )
    description = models.CharField(
        max_length=100,
        db_comment="Description of the Sample",
    )

    class Meta:
        db_table_comment = "Table containing the types a Sample can be"


class SampleStatus(BaseModel):
    # TODO: the different Statuses we can give to a Sample?
    name = models.CharField(
        choices=[
            ("RECEIVED", "Received"),
            ("PENDING", "Pending"),
            ("ARCHIVED", "Archived"),
            ("TESTED", "Tested"),
            ("ACCEPTED", "Accepted"),
            ("REJECTED", "Rejected"),
        ],
        max_length=30,
        db_comment="What is the status of the Sample",
    )  # received, pending,
    # TODO: what are the other fields we can store here ?
    # owner ? next step ?


class Project(BaseModel):
    name = models.CharField(max_length=100, db_comment="Name of the Project")
    description = models.TextField(db_comment="Description of the Project")
    start_date = models.DateField(db_comment="Starting date for the Project")
    end_date = models.DateField(
        db_comment="Ending date for the Project", null=True, blank=True
    )
    status = models.CharField(max_length=50, db_comment="Status of the Project")

    class Meta:
        db_table_comment = "List all the Projects a Sample could be linked to"


class Sample(BaseModel):
    material = models.ForeignKey(
        "SampleMaterial",
        on_delete=models.SET_NULL,
        db_comment="The Sample type, the Sample nature, ...",
        null=True,
        blank=True,
    )
    project = models.ForeignKey(
        "Project",
        on_delete=models.SET_NULL,
        db_comment="The Sample was collected for which Project?",
        null=True,
        blank=True,
    )
    # Reason for the Sample
    context = models.CharField(
        choices=[
            ("MEDICAL EMERGENCY", "Medical emergency"),
            ("EPIDEMY", "Epidemy"),
            ("PROFESSIONAL", "Professional"),
            ("PROJECT", "Project"),
        ],
        max_length=50,
        default="PROJECT",
        db_comment="For what reason the Sample was collected",
    )
    status = models.ForeignKey(
        "SampleStatus",
        on_delete=models.SET_NULL,
        db_comment="Sample current status: pending? received? analysis? archived? ...",
        null=True,
        blank=True,
    )
    origin = models.ForeignKey(
        "places.Location",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        db_comment="Location where sample was collected",
    )

    # a method that will retrieve all the criteria for this sample for conformity
    def conformity_criterias(self):
        pass

    # a method that will retrieve all the criteria for this sample for acceptability
    def acceptability_criteria(self):
        pass

    def is_conformant(self):
        # select inside the SampleCriteriaMapping table
        # if all conformity criterias are True, return True
        # else return False
        pass

    def is_accepted(self):
        # if is conformant, return True
        # if not conformant...
        # select inside the SampleCriteriaMapping table
        # if all acceptability criterias are True, return True
        # else return False
        pass

    def is_rejected(self):
        pass

    class Meta:
        db_table_comment = "List of all the Samples inside the database"


class SampleLaboratoryMapping(BaseModel):
    sample = models.ForeignKey(
        "Sample", on_delete=models.CASCADE, db_comment="Sample instance"
    )
    laboratory = models.ForeignKey(
        "Laboratory",
        on_delete=models.CASCADE,
        db_comment="Laboratory where Sample is ent",
    )
    # TODO: should be generated ?
    identifier = models.CharField(
        max_length=20, db_comment="The Sample identifier inside the Laboratory"
    )

    # TODO: Other fields ?
    class Meta:
        db_table_comment = (
            "Table used to determine which Sample is inside which Laboratory"
        )


class Criteria(BaseModel):
    """
    To be registered, a Sample needs to fill some Criteria
    Ex:
    - 01, ACC, is the sample labelled correctly ?
    - 02, CONF, is the sample not broken ?
    - 03, REJ, is the sample stored at the right temp ?
    - etc...
    """

    lab_unit = models.ForeignKey(
        "LaboratoryUnit",
        on_delete=models.CASCADE,
        db_comment="Which LaboratoryUnit is using this Criteria ?",
    )
    classification = models.CharField(
        choices=[
            ("COMPLIANCE", "Compliance"),  # TO BE Checked first
            ("ACCEPTABILITY", "Acceptability"),  # To be checked only if non compliant
            ("REJECTION", "Rejection"),
        ],
        max_length=20,
        db_comment="What is the type of the Criteria",
    )

    reason = models.TextField(db_comment="More detailed description for the criteria")
    code = models.IntegerField(
        db_comment="A code that will help recognize the criteria"
    )

    class Meta:
        db_table_comment = (
            "Criterias used to categorize Sample during Registration process"
        )


class SampleCriteriaMapping(BaseModel):
    sample = models.ForeignKey(
        "Sample", on_delete=models.CASCADE, db_comment="Sample instance ..."
    )
    criteria = models.ForeignKey(
        "Criteria", on_delete=models.CASCADE, db_comment="... has the criteria ..."
    )

    class Meta:
        db_table_comment = "List mappings between Criterias and Samples"


class Equipment(models.Model):
    model = models.CharField(max_length=100)
    serial_number = models.CharField(max_length=50)
    version = models.CharField(max_length=100)
    last_calibration_date = models.DateField()

    class Meta:
        db_table_comment = "various tools used by scientists working in the lab"


class Laboratory(BaseModel):
    name = models.CharField(max_length=100)
    lab_id = models.CharField(max_length=50, unique=True)
    location = models.ForeignKey(
        "places.Location",
        on_delete=models.SET_NULL,
        db_comment="Location where sample was collected",
        null=True,
        blank=True,
    )
    status = models.CharField(
        choices=[
            ("ACTIVE", "Active"),
            ("SUSPENDED", "Suspended"),
            ("INACTIVE", "Inactive"),
        ],
        max_length=20,
        db_comment="The operational status of the Laboratory",
    )

    phone = models.CharField(max_length=20)
    email = models.EmailField()
    address = models.CharField(max_length=255)

    staff = models.ManyToManyField(User)
    equipment = models.ManyToManyField(Equipment)
    projects = models.ManyToManyField(Project)

    # certifications = models.ManyToManyField(Certification)
    # operating_hours = models.ManyToManyField(OperatingHours)

    class Meta:
        db_table_comment = "Building equipped for scientific experiments"


class LaboratoryUnit(BaseModel):
    # Inside a Laboratory there are many Units
    laboratory = models.ForeignKey(
        "Laboratory",
        on_delete=models.SET_NULL,
        db_comment="Laboratory",
        null=True,
        blank=True,
    )
    name = models.CharField(
        max_length=20,
        db_comment="Name of the Laboratory's unit",
    )

    # TODO: any other fields ?
    class Meta:
        db_table_comment = "Table containing which Unit exists in which Lab"


class SampleLaboratoryMapping(BaseModel):
    laboratory = models.ForeignKey(
        "Laboratory",
        on_delete=models.CASCADE,
        db_comment="Laboratory",
    )
    sample = models.ForeignKey(
        "Sample",
        on_delete=models.CASCADE,
        db_comment="Sample",
    )

    class Meta:
        db_table_comment = "Which Sample is sent to which Lab?"


class Registration(BaseModel):
    user = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        db_comment="The user doing the registration Sample",
    )
    sample = models.ForeignKey(
        "Sample",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        db_comment="The Sample being registered",
    )
    reception_date = models.DateTimeField(
        db_comment="Date and time of the Sample registration"
    )
    # TODO, find a way to link SampleCriteriaMapping

    class Meta:
        db_table_comment = "Who registered, what Sample, when?"


class Disease(BaseModel):
    # Name: The name of the disease.
    # Cause: The cause of the disease (e.g., pathogen, genetic, environmental).
    # Symptoms: The symptoms associated with the disease.
    # Onset: The typical onset of the disease (e.g., acute, chronic).
    # Duration: The typical duration of the disease.
    # Severity: The severity of the disease (e.g., mild, moderate, severe).
    # Transmission: The mode of transmission of the disease (e.g., airborne, waterborne, vector-borne, sexual).
    # Incubation period: The time between infection and the onset of symptoms.
    # Diagnosis: The recommended method for diagnosing the disease.
    # Treatment: The recommended treatment for the disease.
    # Prevention: The recommended methods for preventing the disease.
    # Prognosis: The expected outcome of the disease.
    pass


class Pathogen(BaseModel):
    # an organism causing disease to its host,
    # with the severity of the disease symptoms referred to as virulence
    # for each pathogen, there is a list of test that can be done

    # The agents that cause disease fall into five groups:
    # viruses, bacteria, fungi, protozoa, and helminths (worms).

    # Name: The name of the pathogen.
    # Type: The type of pathogen (e.g., virus, bacteria, fungus, parasite).
    # Genome: The genome of the pathogen (e.g., DNA, RNA).
    # Strain: The specific strain of the pathogen.
    # Virulence: The degree of pathogenicity of the pathogen.
    # Transmission: The mode of transmission of the pathogen (e.g., airborne, waterborne, vector-borne, sexual).
    # Host range: The range of hosts that the pathogen can infect.
    # Reservoir: The natural reservoir of the pathogen (e.g., animals, humans, environment).
    # Incubation period: The time between infection and the onset of symptoms.
    # Treatment: The recommended treatment for the infection caused by the pathogen.
    pass


class Analysis(BaseModel):
    sample = models.ForeignKey(
        "Sample",
        on_delete=models.CASCADE,
        db_comment="The Sample for which the analysis is asked",
    )
    for_pathogen = models.ForeignKey(
        "Pathogen",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        db_comment="The Pathogen for which the analysis is asked",
    )
    result = models.CharField(
        choices=[
            ("NEGATIVE", "Negative"),
            ("POSITIVE", "Positive"),
            ("NEUTRAL", "Neutral"),
            ("UNKNOWN", "Unknown"),
        ],
        max_length=10,
        db_comment="Is the result positive ? Negative ? Unknown ?",
    )

    # TODO: any other values to store here ?
    class Meta:
        db_table_comment = "Detailed examination of the Sample"
