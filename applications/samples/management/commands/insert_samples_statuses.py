import datetime

from django.core.management.base import BaseCommand

from applications.places.models import Location
from applications.samples.models import Project, Sample, SampleMaterial, SampleStatus


class Command(BaseCommand):
    help = "Creates and inserts one Project and Sample statuses inside the database"

    def handle(self, *args, **options):
        # insert projects
        Project.objects.get_or_create(
            name="First project",
            description="Description of the first project",
            start_date=datetime.datetime.today(),
            end_date=datetime.datetime.today(),
            status="In progress",
        )

        # insert samples statuses
        statuses = [
            "RECEIVED",
            "PENDING",
            "ARCHIVED",
            "TESTED",
            "ACCEPTED",
            "REJECTED",
        ]
        for status in statuses:
            SampleStatus.objects.get_or_create(name=status)

        self.stdout.write(
            self.style.SUCCESS(
                "Successfully inserted one Project and Sample Statuses inside database"
            )
        )
