from django.core.management.base import BaseCommand

from applications.samples.models import SampleMaterial


class Command(BaseCommand):
    help = "Creates and inserts Sample materials inside the database"

    def handle(self, *args, **options):
        materials = [
            ("HUMAN", "BLOOD", "blood sample taken from a human being"),
            ("HUMAN", "SPUTUM", "sputum sample taken from a human being"),
            ("HUMAN", "PLEURAL_FLUID", "pleural fluid taken from a human being"),
            ("OTHER", "OTHER", "other"),
        ]
        for c, s, d in materials:
            SampleMaterial.objects.get_or_create(
                category=c, subcategory=s, description=d
            )

        self.stdout.write(
            self.style.SUCCESS("Successfully inserted Sample materials inside database")
        )
