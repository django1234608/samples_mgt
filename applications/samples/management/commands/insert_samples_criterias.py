from django.core.management.base import BaseCommand

from applications.samples.models import Criteria, Laboratory, LaboratoryUnit


class Command(BaseCommand):
    help = "Creates and inserts Criterias inside the database"

    def handle(self, *args, **options):
        # create one Laboratory and one Laboratory Unit
        lab, created = Laboratory.objects.get_or_create(
            name="First Laboratory",
            lab_id="lab unique identifier",
            status="ACTIVE",
            phone="+221781450048",
            email="ptrck@nskm.xyz",
            address="Dakar Senegal",
        )
        lab_unit, created = LaboratoryUnit.objects.get_or_create(
            laboratory=lab,
            name="Lab Unit 1",
        )
        # TODO: insert Criterias
        criterias = [
            (lab_unit, "COMPLIANCE", "reason 1", 100),
            (lab_unit, "ACCEPTABILITY", "reason 2", 200),
            (lab_unit, "REJECTION", "reason 3", 300),
        ]
        for l, c, r, d in criterias:
            Criteria.objects.get_or_create(
                lab_unit=l, classification=c, reason=r, code=d
            )
        self.stdout.write(
            self.style.SUCCESS(
                "Success inserting Lab, LabUnit & Criterias inside database"
            )
        )
