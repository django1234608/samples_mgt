import datetime
import io

import polars as pl
from django.db import connection
from django.http import FileResponse
from django.utils.translation import gettext as _
from drf_spectacular.utils import extend_schema
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from rest_framework.response import Response
from rest_framework.views import APIView

# Create your views here.


class ExampleView(APIView):
    def get(self, request):
        content = {
            "user": str(request.user),  # `django.contrib.auth.User` instance.
            "auth": str(request.auth),  # None
            "message": _("This is a dummy message"),
        }
        return Response(content)
