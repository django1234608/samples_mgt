import json

import pytest
from django.apps import apps
from django.utils.translation import activate, deactivate
from django.utils.translation import gettext as _
from rest_framework.test import APIRequestFactory, force_authenticate

from applications.patients.api.v1 import views
from applications.samples.api.v1.tests.fixtures import one_user

patients_config = apps.get_app_config("patients")


@pytest.mark.django_db
def test_translations():
    activate("es")
    translated_text = _("This is a dummy message")
    assert translated_text == "Éste es un mensaje ficticio"
    activate("fr")
    translated_text = _("This is a dummy message")
    assert translated_text == "Ceci est un message factice"
    activate("en")
    translated_text = _("This is a dummy message")
    assert translated_text == "This is a dummy message"
    deactivate()


@pytest.mark.django_db
def test_get_example_view_in_french(one_user):
    # curl -L -XGET
    #      -H"Content-Type: application/json"
    #      -H"Authorization: Bearer token"
    #      -H"Accept-Language: es" http://localhost:8000/api/v1/patients/example/

    # {"user":"nsukami","auth":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNzE4ODk0NjEzLCJpYXQiOjE3MTg4OTI4MTMsImp0aSI6IjQ5ZTE4NDgxZmE1OTRjMGM5MWQ5NTEyYmQ1YTNhYmZmIiwidXNlcl9pZCI6MX0.PdCTT4gcBTC7w2mKADSzcncLMCbbRPIiPJiULVARMqo","message":"Éste es un mensaje ficticio"

    activate("fr")  # activate french translation
    factory = APIRequestFactory()
    view = views.ExampleView.as_view()
    request = factory.get(
        "/api/v1/patients/example/",
        content_type="application/json",
        HTTP_ACCEPT_LANGUAGE="fr",  # TODO: seems useless
    )
    force_authenticate(request, one_user)
    response = view(request).render()
    translated_text = _("This is a dummy message")
    deactivate()  # deactivate french translation
    assert translated_text == "Ceci est un message factice"
    assert json.loads(response.content.decode())["message"] == translated_text
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "application/json"
