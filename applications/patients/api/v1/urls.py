"""
URL configuration for patients Rest API v1
"""

from django.urls import path

from applications.patients.api.v1 import views

app_name = "applications.patients"

example_detail = views.ExampleView.as_view()

urlpatterns = [
    path("patients/example/", example_detail, name="example-detail"),
]
