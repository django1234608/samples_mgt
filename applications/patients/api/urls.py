"""
patients all Rest API URLs.
"""

from django.urls import include, path

app_name = "applications.patients"

urlpatterns = [
    path("v1/", include("applications.patients.api.v1.urls", namespace="v1"))
]
