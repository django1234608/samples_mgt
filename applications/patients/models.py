from django.db import models

from applications.core.models import BaseModel


class Patient(BaseModel):
    firstname = models.CharField(
        max_length=100,
        db_comment="Patient's first name",
    )
    lastname = models.CharField(
        max_length=100,
        db_comment="Patient's last name",
    )
    salutation = models.CharField(
        max_length=5,
        choices=[("MR", "MR"), ("MRS", "MRS"), ("OTHER", "OTHER")],
        db_comment="How the patient should be greet",
        default="OTHER",
        null=True,
        blank=True,
    )
    sex = models.CharField(
        max_length=10,
        choices=[("MALE", "Male"), ("FEMALE", "Female")],
        db_comment="Patient's sex, possible values are 'M' or 'F'",
        default="F",
        null=True,
        blank=True,
    )
    birth_date = models.DateField(
        db_comment="Date of birth of the Patient",
        null=True,
        blank=True,
    )
    place_of_birth_community = models.ForeignKey(
        "places.Community",
        on_delete=models.SET_NULL,
        db_comment="Where the patient is born",
        null=True,
        blank=True,
    )
    place_of_birth_department = models.ForeignKey(
        "places.Department",
        on_delete=models.SET_NULL,
        db_comment="Where the patient is born",
        null=True,
        blank=True,
    )
    place_of_birth_region = models.ForeignKey(
        "places.Region",
        on_delete=models.SET_NULL,
        db_comment="Where the patient is born",
        null=True,
        blank=True,
    )
    approximate_age = models.IntegerField(
        db_comment="Age of the Patient, approximatively, when unknown birth date",
        null=True,
        blank=True,
    )
    approximate_age_type = models.CharField(
        max_length=10,
        choices=[("DAYS", "Days"), ("MONTHS", "Months"), ("YEARS", "Years")],
        db_comment="Age of the Patient, approximatively, in days ? months ? or years ?",
        null=True,
        blank=True,
    )

    nationality = models.CharField(
        max_length=100,
        db_comment="Patient's country of origin",
    )
    marital_status = models.CharField(
        max_length=10,
        choices=[("MARIED", "Maried"), ("SINGLE", "Single"), ("UNKNOWN", "Unknown")],
        default="U",
        db_comment="Relationship with a significant other: 'Maried', 'Single', 'Unknown'",
    )
    email = models.CharField(
        max_length=100,
        db_comment="Patient's email address",
    )
    phone = models.CharField(
        max_length=15,
        db_comment="Patient's telephone number",
    )
    address = models.CharField(
        max_length=100,
        db_comment="Patient's address",
    )
    emergency_contact = models.CharField(
        max_length=100,
        db_comment="Individual medics will get in touch with in an emergency",
    )
    insurance_information = models.CharField(
        max_length=100,
        db_comment="Patient's health insurance information",
    )
    primary_physician = models.CharField(
        max_length=100,
        db_comment="Informations about the Patient's primary care provider",
    )
    clinical_data = models.JSONField(
        db_comment="medical history, symptoms, physical exam findings...",
    )
    epidemiological_data = models.JSONField(
        db_comment="race, geographic location, lifestyle habits, environmental exposures..."
    )

    class Meta:
        db_table_comment = "Table used to store Patient's informations"
