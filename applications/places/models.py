from django.db import models

from applications.core.models import BaseModel


class Region(BaseModel):  # Region, contient +ieurs dept
    name = models.CharField(
        max_length=20,
        db_comment="One of the Senegal's Regions",
    )
    # TODO: should be calculated based on the Region's name
    iso_code = models.CharField(
        max_length=5,
        db_comment="Region's ISO code",
    )

    class Meta:
        db_table_comment = "Senegal is divided in Regions, for example: "


class Department(BaseModel):  # Département, contient +ieurs arrondissemnet
    region = models.ForeignKey(
        "Region",
        on_delete=models.CASCADE,
        db_comment="A Department belongs to a Region",
    )
    name = models.CharField(
        max_length=20,
        db_comment="One of the Senegal's Departments",
    )
    # TODO: should be calculated based on the Dept's name
    iso_code = models.CharField(
        max_length=5,
        db_comment="Department's ISO code",
    )

    class Meta:
        db_table_comment = "Senegal is divided in Departments, for example: "
        unique_together = ["region", "name"]


class Borough(BaseModel):  # Arrondissement, contient +ieurs communes
    department = models.ForeignKey(
        "Department",
        on_delete=models.CASCADE,
        db_comment="A Borough belongs to a Department",
    )
    # TODO: should be restricted based on the foreign key
    name = models.CharField(
        max_length=20,
        db_comment="One of the Senegal's Boroughs (arrondissements)",
    )
    # TODO: should be calculated based on the Dept's name
    iso_code = models.CharField(
        max_length=5,
        db_comment="Department's ISO code",
    )

    class Meta:
        db_table_comment = "Senegal is divided in Departments, for example: "
        unique_together = ["department", "name"]


class Community(BaseModel):  # Commune/Communauté rurale/municipalité
    borough = models.ForeignKey(
        "Borough",
        on_delete=models.CASCADE,
        db_comment="A Municipality(Commune) belongs to a Borough(Arrondissement)",
    )
    # TODO: should be restricted based on the foreign key
    name = models.CharField(
        max_length=20,
        db_comment="One of the Senegal's Boroughs (communes)",
    )
    # TODO: should be calculated based on the Dept's name
    iso_code = models.CharField(
        max_length=5,
        db_comment="Department's ISO code",
    )

    class Meta:
        db_table_comment = "Senegal is divided in Municipalities, for example: "
        unique_together = ["borough", "name"]


class Location(BaseModel):
    # TODO: should be restricted
    continent = models.CharField(
        max_length=20,
        db_comment="Continent's name, for example: Africa, South America, Antartica, ...",
    )
    # TODO: should be restricted based on continent
    country = models.CharField(
        max_length=20,
        db_comment="Continent's name, for example: Africa, South America, Antartica, ...",
    )
    # TODO: should be restricted based on country
    region = models.ForeignKey(
        "Region", on_delete=models.CASCADE, db_comment="A Region", null=True, blank=True
    )
    department = models.ForeignKey(
        "Department",
        on_delete=models.CASCADE,
        db_comment="A Department",
        null=True,
        blank=True,
    )
    borough = models.ForeignKey(
        "Borough",
        on_delete=models.CASCADE,
        db_comment="A Borough (Un Arrondissement)",
        null=True,
        blank=True,
    )
    community = models.ForeignKey(
        "Community",
        on_delete=models.CASCADE,
        db_comment="A Municipality (Commune, communauté rurale)",
        null=True,
        blank=True,
    )
    # TODO: explore PointField
    # https://docs.djangoproject.com/en/5.0/ref/contrib/gis/model-api/#pointfield
    latitude = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        db_comment="What is the latitude of this location?",
    )
    longitude = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        db_comment="What is the longitude of this location?",
    )
    details = models.CharField(
        max_length=100,
        db_comment="Any particular detail about the location.",
    )

    class Meta:
        db_table_comment = "Any location anywhere in the world"
