from pathlib import Path

from django.apps import AppConfig
from django.conf import settings


class PlacesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "applications.places"
    PLACES_BASE_DIR = Path(__file__).resolve().parent
    DEFAULT_DATA_DIR = PLACES_BASE_DIR / "data"
    DATA_DIR = getattr(settings, "DATA_DIR", DEFAULT_DATA_DIR)
