"""
URL configuration for places Rest API v1
"""

from django.urls import path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

from applications.places.api.v1 import views

app_name = "applications.places"

urlpatterns = []
