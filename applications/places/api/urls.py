"""
places all Rest API URLs.
"""

from django.urls import include, path

app_name = "applications.places"

urlpatterns = [path("v1/", include("applications.places.api.v1.urls", namespace="v1"))]
