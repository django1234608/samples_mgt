from django.urls import include, path
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from applications.core.api.v1.views import data_dictionary

# include your v1 urls here
urlpatterns = [
    path("v1/", include("applications.patients.api.v1.urls")),
    path("v1/", include("applications.samples.api.v1.urls")),
]

# Tokens
urlpatterns += [
    # JWT
    path("v1/token/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("v1/token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
]

# Core
urlpatterns += [
    path("v1/data/dictionary", data_dictionary, name="database-data-dictionary"),
]


# Swagger
urlpatterns += [
    path("v1/schema/", SpectacularAPIView.as_view(), name="schema"),
    # Optional UI:
    path(
        "v1/schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="v1:schema"),
        name="swagger-ui",
    ),
    path(
        "v1/schema/redoc/",
        SpectacularRedocView.as_view(url_name="v1:schema"),
        name="redoc",
    ),
]
